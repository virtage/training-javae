package cz.virtage.edu.javae.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 */
@WebServlet("/GreeterServlet")
public class GreeterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");

		String first = request.getParameter("first");
		String last = request.getParameter("last");

		if ((first == null) || (last == null)) {
			response.getWriter().println("<h1>Hello anonymous!</h1>");
			
		} else {
			response.getWriter().println(
					"<h1>Hello " + first + " " + last + "!</h1>");
		}

	}

}
