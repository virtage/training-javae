package cz.virtage.edu.javae.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class GreetingHandler extends DefaultHandler {
	
	private StringBuffer firstName = new StringBuffer();
	private StringBuffer lastName = new StringBuffer();
	private boolean readingFirstName;
	private boolean readingLastName;
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		if (qName.equals("first")) {
			readingFirstName = true;
		}
		
		if (qName.equals("last")) {
			readingLastName = true;
		}
		
		if (qName.equals("male") && (firstName != null) && (lastName != null)) {
			System.out.format("Hello Mr. %s %s!%n", firstName, lastName);
		}
		
		if (qName.equals("female") && (firstName != null) && (lastName != null)) {
			System.out.format("Hello Mrs. %s %s!%n", firstName, lastName);
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		if (qName.equals("first")) {
			readingFirstName = false;
		}
		
		if (qName.equals("last")) {
			readingLastName = false;
		}

	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		
		if (readingFirstName) {
			firstName.append(ch, start, length);
		}
		
		if (readingLastName) {
			lastName.append(ch, start, length);
		}
	}
	
}