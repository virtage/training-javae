package cz.virtage.edu.javae.jpa;


import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import cz.virtage.edu.javae.jpa.course.Jazyk;
import cz.virtage.edu.javae.jpa.course.Kategorie;
import cz.virtage.edu.javae.jpa.course.Kurz;

public class Jpa1 {

	// Název PU musí korespondovat s názvem v ``META-INF/persistence.xml``
	private static final String PERSISTENCE_UNIT = "vskoleni";
	
	public static void main(String[] args) {
		// ######################
		// Získání EntityManageru 
		// ######################
		
		// V prostředí Java SE (mimo aplikační server):
		EntityManagerFactory factory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
		EntityManager em = factory.createEntityManager();

		// #########
		// Vytváření
		// #########
		
		// Začneme transakci
		em.getTransaction().begin();
		
		// Vytvoříme si všechno potřebné
		Kategorie kategorieJava = new Kategorie();
		kategorieJava.setKod("JAVA");
		
		Kategorie kategorieEclipse = new Kategorie();
		kategorieEclipse.setKod("ECLIPSE");
		
		
		Jazyk cesky = new Jazyk();
		cesky.setNazev("český");
		
		Jazyk anglicky = new Jazyk();
		anglicky.setNazev("anglický");
		
		Kurz EIDE = new Kurz();
		EIDE.setKod("EIDE");
		EIDE.setNazev("Eclipse IDE");
		EIDE.setJazyky(Arrays.asList(cesky, anglicky));
		EIDE.setKategorie(kategorieEclipse);
		
		Kurz MVN = new Kurz();
		MVN.setKod("MVN");
		MVN.setNazev("Maven");
		MVN.setJazyky(Arrays.asList(cesky, anglicky));
		MVN.setKategorie(kategorieJava);

		Kurz JAVAZ = new Kurz();
		JAVAZ.setKod("JAVAZ");
		JAVAZ.setNazev("Java základní kurz");
		JAVAZ.setJazyky(Arrays.asList(cesky, anglicky));
		JAVAZ.setNavazKurzy(Arrays.asList(EIDE, MVN));
		JAVAZ.setKategorie(kategorieJava);

		
		// Pole kategorie kurzu není záměrně kaskádováno na PERSIST operaci
		em.persist(kategorieEclipse);
		em.persist(kategorieJava);

		// Ostatní vztahy třídy Kurz na další vazební objekty
		// (jazyky, předcházející kurzy) se také uloží automaticky díky parametru
		// ``cascade=CascadeType.ALL`` anotací @OneToMany, @ManyToMany ap.
		em.persist(JAVAZ);
		

		
		// Potvrdíme transakci
		em.getTransaction().commit();

		// ##########
		// Dotazování
		// ##########
		
		// Výběr (select)
		// **************
		// Netypovaný dotaz Query 
		Query query = em.createQuery("select k from Kurz k");
		List list1 = query.getResultList(); 	// vrací List, nikoli List<Kurz>
		
		// Když to jde, použijte TypedQuery<T>
		TypedQuery<Kurz> typedQuery = em.createQuery("select k from Kurz k", Kurz.class);
		List<Kurz> list2 = typedQuery.getResultList(); // vrací List<Kurz>
		
		
		System.out.println("**** Všechny kurzy ****");
		for (Kurz k : list2) {
			System.out.println(k);
		}
		System.out.println("Počet: " + list2.size());
		
		
		System.out.println("**** Kurzy v kategorii JAVA ****");
		TypedQuery<Kurz> q3 = em.createQuery("select k from Kurz k where k.kategorie = ?1", Kurz.class);
		q3.setParameter(1, kategorieJava);
		List<Kurz> list3 = q3.getResultList();
		
		for (Kurz k : list3) {
			System.out.println(k);
		}
		System.out.println("Počet: " + list2.size());

		// #################
		// Konec práce s JPA
		// #################
		em.close();
		factory.close();
	}
}
