package cz.virtage.edu.javae.sax;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class SaxGreeter1 extends DefaultHandler {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		
		SAXParserFactory spf = SAXParserFactory.newInstance();
		spf.setValidating(false);
		
		SAXParser saxParser = spf.newSAXParser();
		XMLReader parser = saxParser.getXMLReader();
				
		parser.setContentHandler(new LoggingHandler());
		
		parser.parse("/absolute/path/to/person.xml");
	}
	
}
