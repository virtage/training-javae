package cz.virtage.edu.javae.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JdbcBasics {
	
	public static void main(String[] args) throws SQLException {
		viewTable(getConnection());
	}
	
	public static Connection getConnection() {

		Properties connectionProps = new Properties();
		connectionProps.put("user", "change");
		connectionProps.put("password", "credentials");
		
		try (Connection conn = DriverManager.getConnection("jdbc:mariadb://localhost:3306/sakila",
						connectionProps)) {
			System.out.println("Connected to database");
			return conn;
			
		} catch (SQLException e) {
			System.out.println("Failed to create connection" + e.getMessage());
			return null;
			
		}
	}

	public static void viewTable(Connection conn) {
		String sql = "select * from city";

		try (Statement stmt = conn.createStatement()) {
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				System.out.println("city_id = " + rs.getString("CITY_ID")
						+ ", city = " + rs.getString("CITY"));
				
			}
			
		} catch (SQLException e) {
			System.out.println("Failed to create statement: " + e.getMessage());
			
		}
	}
}
