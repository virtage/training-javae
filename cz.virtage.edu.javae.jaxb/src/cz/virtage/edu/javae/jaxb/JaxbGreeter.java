package cz.virtage.edu.javae.jaxb;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import cz.virtage.edu.javae.jaxb.person.PersonType;

public class JaxbGreeter {

	public static void main(String[] args) throws JAXBException {

		// ** Unmarshall **
		JAXBContext jaxbContext = JAXBContext
				.newInstance("cz.virtage.edu.javae.jaxb.person");
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		@SuppressWarnings("unchecked")
		JAXBElement<PersonType> person = (JAXBElement<PersonType>) unmarshaller
				.unmarshal(new File(
						"/absolute/path/to/person.xml"));

		PersonType personType = (PersonType) person.getValue();

		String name = personType.getName().getFirst() + " "
				+ personType.getName().getLast();

		if (personType.getSex().getMale() != null) {
			System.out.println("Hello Mr. " + name);
		}

		if (personType.getSex().getFemale() != null) {
			System.out.println("Hello Mrs. " + name);
		}

	}

}
