package cz.virtage.edu.javae.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class LoggingHandler extends DefaultHandler {
	
	@Override
	public void startDocument() throws SAXException {
		System.out.format("startDocument()%n");
	}
	
	@Override
	public void endDocument() throws SAXException {
		System.out.format("endDocument()%n");
	}
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		System.out.format("startElement(): %s%n", qName);
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		System.out.format("endElement(): %s%n", qName);
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		System.out.format("characters()%n");
	}

	
}
