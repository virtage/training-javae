package cz.virtage.edu.javae.jpa.course;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;


@Entity
public class Kurz {
	
	// Primární klíč
	@Id
	private String kod;
	
	private String nazev;
	
	// Vazba 1:N.
	// Kurz je owning-side, proto @ManyToOne a ne @OneToMany.
	// Záměrně chybí kaskádování
	@ManyToOne
	private Kategorie kategorie;
	
	// Vazba N:M jednosměrná.
	// Provést všechny úpravy i na navazbených entitách
	@ManyToMany(cascade=CascadeType.ALL)
	// Lepší název vazební tabulky, a jejich sloupců
	@JoinTable(name="KURZ_JAZYKY",
		joinColumns=@JoinColumn(name="KURZ_KOD"),
		inverseJoinColumns=@JoinColumn(name="JAZYK_NAZEV")
	)
	private List<Jazyk> jazyky;
	
	// Vazba N:M jednosměrná.
	// Provést všechny úpravy i na navazbených entitách
	@ManyToMany(cascade=CascadeType.ALL)
	// Lepší název vazební tabulky, a jejich sloupců
	@JoinTable(name="NAVAZ_KURZY",
		joinColumns=@JoinColumn(name="KURZ_KOD"),
		inverseJoinColumns=@JoinColumn(name="NAVAZ_KURZ_KOD")
	)
	private List<Kurz> navazKurzy;
	
	
	public String getKod() {
		return kod;
	}
	
	public void setKod(String kod) {
		this.kod = kod;
	}
	
	public String getNazev() {
		return nazev;
	}
	
	public void setNazev(String nazev) {
		this.nazev = nazev;
	}
	
	public Kategorie getKategorie() {
		return kategorie;
	}
	
	public void setKategorie(Kategorie kategorie) {
		this.kategorie = kategorie;
	}
	
	public List<Jazyk> getJazyky() {
		return jazyky;
	}
	
	public void setJazyky(List<Jazyk> jazyky) {
		this.jazyky = jazyky;
	}
	
	public List<Kurz> getNavazKurzy() {
		return navazKurzy;
	}
	
	public void setNavazKurzy(List<Kurz> navazKurzy) {
		this.navazKurzy = navazKurzy;
	}

	@Override
	public String toString() {
		return "Kurz [kod=" + kod + ", nazev=" + nazev + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kod == null) ? 0 : kod.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Kurz other = (Kurz) obj;
		if (kod == null) {
			if (other.kod != null)
				return false;
		} else if (!kod.equals(other.kod))
			return false;
		return true;
	}

}
