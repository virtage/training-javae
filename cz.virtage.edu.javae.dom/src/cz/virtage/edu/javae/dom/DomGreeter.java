package cz.virtage.edu.javae.dom;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomGreeter {

	public static void main(String[] args) throws ParserConfigurationException,
			SAXException, IOException {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setValidating(false);
		DocumentBuilder builder = dbf.newDocumentBuilder();
		Document doc = builder
				.parse("/absolute/path/to/person.xml");

		System.out.println(salute(doc));
	}

	private static String salute(Document doc) {
		// Find out name
		String first = doc.getElementsByTagName("first").item(0)
				.getFirstChild().getNodeValue().trim();
		String last = doc.getElementsByTagName("last").item(0).getFirstChild()
				.getNodeValue().trim();

		// Find out sex
		NodeList nodeList = doc.getElementsByTagName("male");
		if (nodeList.getLength() == 1) {
			return "Hello Mr. " + first + " " + last;
		}

		nodeList = doc.getElementsByTagName("female");
		if (nodeList.getLength() == 1) {
			return "Hello Mrs. " + first + " " + last;
		}

		return "Sex not specified";
	}
}
