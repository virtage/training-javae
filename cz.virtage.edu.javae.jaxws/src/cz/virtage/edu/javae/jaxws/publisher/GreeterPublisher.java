package cz.virtage.edu.javae.jaxws.publisher;

import javax.xml.ws.Endpoint;

import cz.virtage.edu.javae.jaxws.service.GreeterImpl;

public class GreeterPublisher {

	public static final String GREETER_URL = "http://localhost:9999/ws/greeter";

	public static void main(String[] args) {
		
		Endpoint.publish(
				GREETER_URL,
				new GreeterImpl());
		
	}
}
