<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<body>

<h1>JSTL showcase</h1>

<h2>Core tags</h2>

<c:set var="age">23</c:set>

<p>Your age is ${age} it means 

<c:if test="${!empty age}">
	<c:choose>
		<c:when test="${age > 18}">
	       you can vote!
	   </c:when>
		<c:when test="${age > 21}">
	       you can be elected!
	   </c:when>
		<c:when test="${age > 40}">
	       you can be elected as senator or president!
	   </c:when>
		<c:otherwise>
	       politics is not for children!
	   </c:otherwise>
	</c:choose>
</c:if>
</p>

<h2>Formatting tags</h2>

<fmt:setLocale value="cs_CZ"/>

<c:set var="now" value="<%= new java.util.Date()%>" />

<p>Current time is <fmt:formatDate value="${now}" type="date" dateStyle="full"/>.</p>

</body>
</html>
