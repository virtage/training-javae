package cz.virtage.edu.javae.jaxws.service;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface Greeter {
	
	@WebMethod
	public String greet(int hour);
	
}
