package cz.virtage.edu.javae.jaxws.service;

import javax.jws.WebService;

@WebService
public class GreeterImpl implements Greeter {
	
	@Override
	public String greet(int hour) {
		if ((hour > 6) && (hour < 12))
			return "Good morning!";
		else if (hour == 12)
			return "Good noon!";
		else if ((hour > 12) && (hour < 17))
			return "Good afternoon!";
		else if ((hour > 17) && (hour < 20))
			return "Good evening!";
		else
			return "Good night!";
	}
}
