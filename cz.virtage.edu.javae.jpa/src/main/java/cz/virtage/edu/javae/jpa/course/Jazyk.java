package cz.virtage.edu.javae.jpa.course;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Jazyk {
	
	@Id
	private String nazev;
	
	public String getNazev() {
		return nazev;
	}
	
	public void setNazev(String nazev) {
		this.nazev = nazev;
	}

	@Override
	public String toString() {
		return "Jazyk [nazev=" + nazev + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nazev == null) ? 0 : nazev.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jazyk other = (Jazyk) obj;
		if (nazev == null) {
			if (other.nazev != null)
				return false;
		} else if (!nazev.equals(other.nazev))
			return false;
		return true;
	}

}
