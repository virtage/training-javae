package cz.virtage.edu.javae.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Greeter service as JAX-RS RESTful service. 
 *
 * @author JELL
 */
@Path("greeter")
public class Greeter {
	
	@GET
	@Path("/{hour}")
	@Produces(MediaType.TEXT_PLAIN)
	public Response greet(@PathParam("hour") int hour) {
		String greeting; 
		
		if ((hour > 6) && (hour < 12)) {
			greeting = "Good morning!";
		} else if (hour == 12) {
			greeting = "Good noon!";
		} else if ((hour > 12) && (hour < 17)) {
			greeting = "Good afternoon!";
		} else if ((hour > 17) && (hour < 20)) {
			greeting = "Good evening!";
		} else {
			greeting = "Good night!";
		}
		
		return Response.status(200).entity(greeting).build();
		
	}
	
}
