package cz.virtage.edu.javae.jpa.course;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Kategorie {
	
	@Id
	private String kod;
	
	public String getKod() {
		return kod;
	}
	
	public void setKod(String kod) {
		this.kod = kod;
	}

}
