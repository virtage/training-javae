package cz.virtage.edu.javae.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import cz.virtage.edu.javae.jaxb.person.MaleType;
import cz.virtage.edu.javae.jaxb.person.NameType;
import cz.virtage.edu.javae.jaxb.person.ObjectFactory;
import cz.virtage.edu.javae.jaxb.person.PersonType;
import cz.virtage.edu.javae.jaxb.person.PhoneType;
import cz.virtage.edu.javae.jaxb.person.SexType;

public class JaxbMarshalExample {
	
	public static void main(String[] args) throws JAXBException {
		
		ObjectFactory of = new ObjectFactory();
		
		// ** Prepare **
		PersonType spiderman = of.createPersonType();
		
		NameType name = of.createNameType();
		name.setFirst("Spider");
		name.setLast("Spiderman");
		
		SexType sex = of.createSexType();
		sex.setMale(new MaleType());
		
		PhoneType phone = of.createPhoneType();
		phone.setPrefix("+200");
		phone.setValue(281900);
		
		// ** Set **
		spiderman.setName(name);
		spiderman.setSex(sex);
		spiderman.setPhone(phone);
		//spiderman.setAge(5);
		spiderman.setJob("Superhero");
		spiderman.setDesc("Always wins");
		
		// ** Marshall **
		JAXBContext jaxbContext = JAXBContext.newInstance("cz.virtage.edu.javae.jaxb.person");
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		JAXBElement<PersonType> jaxbElement = of.createPerson(spiderman);
		marshaller.marshal(jaxbElement, System.out);
		
	}
	
}
