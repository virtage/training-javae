package cz.virtage.edu.javae.jaxws.client;

import java.net.MalformedURLException;
import java.util.Calendar;

public class GreeterClient {
	
	public static void main(String[] args) throws MalformedURLException {
		
		GreeterImplService service = new GreeterImplService();
		GreeterImpl port = service.getGreeterImplPort();
		
		int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
		
		System.out.println(port.greet(currentHour));
		
	}
}
